package com.subastas.activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ExpandableListView.OnGroupCollapseListener;
import android.widget.ExpandableListView.OnGroupExpandListener;
import android.widget.Toast;

import com.subastas.activity.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

public class ConsultaActivity extends ActionBarActivity {
	ProgressDialog prgDialog;
	ExpandableListAdapter listAdapter;
    ExpandableListView expListView;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_consulta);
		prgDialog = new ProgressDialog(this);
		prgDialog.setMessage("Please wait...");
		prgDialog.setCancelable(false);
		expListView = (ExpandableListView) findViewById(R.id.lvExp);
		invokeWS();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.consulta, menu);
		return true;
	}
	public void invokeWS() {
		prgDialog.show();
		final String URL = "http://192.168.1.37:8080/PWsubastas/client/consultas";
		AsyncHttpClient client = new AsyncHttpClient();
        client.get(URL, null ,new AsyncHttpResponseHandler() {
					public void onSuccess(String response) {
						prgDialog.hide();
						loadItems(URL,response);
						Toast.makeText(getApplicationContext(),
								"Estas viendo Consultas!",
								Toast.LENGTH_LONG).show();
					}
					public void onFailure(int statusCode, Throwable error,
							String content) {
						prgDialog.hide();
						if (statusCode == 404) {
							Toast.makeText(getApplicationContext(),
									"Petición de recurso no encontrado",
									Toast.LENGTH_LONG).show();
						} else if (statusCode == 500) {
							Toast.makeText(getApplicationContext(),
									"Algo salió mal en el server end",
									Toast.LENGTH_LONG).show();
						} else {
							Toast.makeText(
									getApplicationContext(),
									"Error inesperado, el servidor podría estar apagado, error de conexión",
									Toast.LENGTH_LONG).show();
						}
					}
				});
	}
	private void loadItems(String URL, String response){
		try {
			listDataHeader = new ArrayList<String>();
		    listDataChild = new HashMap<String, List<String>>();
			
			JSONArray a = new JSONArray(response);
			for (int i = 0; i < a.length(); i++) {
				JSONObject item = a.getJSONObject(i);
				System.out.println(item.get("consultaId"));
				listDataHeader.add(item.get("consultaId").toString()+" "+item.get("nombre").toString());
				List<String> tmp = new ArrayList<String>();
				tmp.add(item.get("email").toString());
				tmp.add(item.get("telefono").toString());
				tmp.add(item.get("comentario").toString());
				listDataChild.put(item.get("consultaId").toString()+" "+item.get("nombre").toString(), tmp);
			}
			listAdapter = new ExpandableListAdapter(this, listDataHeader, listDataChild);
	        expListView.setAdapter(listAdapter);
	        expListView.setOnGroupClickListener(new OnGroupClickListener() {
	            public boolean onGroupClick(ExpandableListView parent, View v,
	                    int groupPosition, long id) {
	                return false;
	            }
	        });
	        expListView.setOnGroupExpandListener(new OnGroupExpandListener() {
	            public void onGroupExpand(int groupPosition) {
	                Toast.makeText(getApplicationContext(),
	                        listDataHeader.get(groupPosition) + " Expanded",
	                        Toast.LENGTH_SHORT).show();
	            }
	        });
	        expListView.setOnGroupCollapseListener(new OnGroupCollapseListener() {
	            public void onGroupCollapse(int groupPosition) {
	                Toast.makeText(getApplicationContext(),
	                        listDataHeader.get(groupPosition) + " Collapsed",
	                        Toast.LENGTH_SHORT).show();
	 
	            }
	        });
	        expListView.setOnChildClickListener(new OnChildClickListener() {
	            public boolean onChildClick(ExpandableListView parent, View v,
	                    int groupPosition, int childPosition, long id) {
	                Toast.makeText(
	                        getApplicationContext(),
	                        listDataHeader.get(groupPosition)
	                                + " : "
	                                + listDataChild.get(
	                                        listDataHeader.get(groupPosition)).get(
	                                        childPosition), Toast.LENGTH_SHORT)
	                        .show();
	                return false;
	            }
	        });
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
