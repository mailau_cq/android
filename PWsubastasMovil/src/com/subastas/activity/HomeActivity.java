package com.subastas.activity;

import com.subastas.activity.R;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

public class HomeActivity extends ActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);
		Intent intent = getIntent();
	    String message = intent.getStringExtra(LoginActivity.EXTRA_MESSAGE);
	    TextView textView=new TextView(this); 
	    textView = (TextView)findViewById(R.id.welcomeText);
	    textView.setText(textView.getText()+" "+message);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.home, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	public void navigatetoConsultaActivity(View view) {
		Intent consultaIntent = new Intent(getApplicationContext(),
				ConsultaActivity.class);
		// Clears History of Activity
		consultaIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(consultaIntent);
	}
}
