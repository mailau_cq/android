package com.subastas.activity;

import java.io.UnsupportedEncodingException;

import org.apache.http.entity.StringEntity;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.subastas.activity.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

public class LoginActivity extends ActionBarActivity {
	public final static String EXTRA_MESSAGE = "com.subastas.activity.MESSAGE";
    ProgressDialog prgDialog;
    TextView errorMsg;
    EditText nameET;
    EditText pwdET;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		errorMsg = (TextView)findViewById(R.id.login_error);
        nameET = (EditText)findViewById(R.id.loginEmail);
        pwdET = (EditText)findViewById(R.id.loginPassword);
        prgDialog = new ProgressDialog(this);
        prgDialog.setMessage("Please wait...");
        prgDialog.setCancelable(false);
	}

	public void loginUser(View view){
        String name = nameET.getText().toString();
        String password = pwdET.getText().toString();
        StringEntity entity = null;
		if (Utility.isNotNull(name) && Utility.isNotNull(password)) {
			try {
				JSONObject usuario = new JSONObject();
				usuario.put("usuarioNombre", name);
				usuario.put("usuarioPassword", password);
				entity = new StringEntity(usuario.toString());
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			} catch (JSONException e) {
				e.printStackTrace();
			}
			invokeWS(entity);
        } else{
            Toast.makeText(getApplicationContext(), "Please fill the form, don't leave any field blank", Toast.LENGTH_LONG).show();
        }
 
    }
	
	public void invokeWS(StringEntity entity) {
		prgDialog.show();
		final String URL = "http://192.168.1.37:8080/PWsubastas/client/usuarios/login";
		AsyncHttpClient client = new AsyncHttpClient();
		client.post(null, URL, entity, "application/json",
				new AsyncHttpResponseHandler() {
					@Override
					public void onSuccess(String response) {
						prgDialog.hide();
						Toast.makeText(getApplicationContext(), "Te haz logueado exitósamente!", Toast.LENGTH_LONG).show();
                        navigatetoHomeActivity();
					}
					@Override
					public void onFailure(int statusCode, Throwable error,
							String content) {
						prgDialog.hide();
						if (statusCode == 404) {
							Toast.makeText(getApplicationContext(),
									"Petición no encontrada",
									Toast.LENGTH_LONG).show();
						} else if (statusCode == 500) {
							Toast.makeText(getApplicationContext(),
									"Algo está mal en el servidor",
									Toast.LENGTH_LONG).show();
						} else {
							Toast.makeText(
									getApplicationContext(),
									"Error inesperado]",
									Toast.LENGTH_LONG).show();
						}
					}
				});
    }
	
	public void navigatetoHomeActivity(){
        Intent homeIntent = new Intent(getApplicationContext(),HomeActivity.class);
        homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        EditText editText = (EditText) findViewById(R.id.loginEmail);
	    String message = editText.getText().toString();
	    homeIntent.putExtra(EXTRA_MESSAGE, message);
        startActivity(homeIntent);
    }
	public void navigatetoRegisterActivity(View view){
        Intent loginIntent = new Intent(getApplicationContext(),RegisterActivity.class);
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(loginIntent);
    }
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.login, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
