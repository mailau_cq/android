package com.subastas.activity;

import java.io.UnsupportedEncodingException;

import org.apache.http.entity.StringEntity;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.subastas.activity.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

public class RegisterActivity extends ActionBarActivity {
	ProgressDialog prgDialog;
	TextView errorMsg;
	EditText nameET;
	EditText addressET;
	EditText pwdET;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register);
		errorMsg = (TextView) findViewById(R.id.register_error);
		nameET = (EditText) findViewById(R.id.registerName);
		addressET = (EditText) findViewById(R.id.registerAddress);
		pwdET = (EditText) findViewById(R.id.registerPassword);
		prgDialog = new ProgressDialog(this);
		prgDialog.setMessage("Please wait...");
		prgDialog.setCancelable(false);
	}

	public void registerUser(View view) {
		String name = nameET.getText().toString();
		String direccion = addressET.getText().toString();
		String password = pwdET.getText().toString();
		StringEntity entity = null;
		if (Utility.isNotNull(name) && Utility.isNotNull(password)) {
			try {
				JSONObject usuario = new JSONObject();
				usuario.put("usuarioNombre", name);
				usuario.put("usuarioPassword", password);
				usuario.put("usuarioDireccion", direccion);
				entity = new StringEntity(usuario.toString());
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			} catch (JSONException e) {
				e.printStackTrace();
			}

			registerWebService(entity);
		} else {
			Toast.makeText(getApplicationContext(),
					"Please fill the form, don't leave any field blank",
					Toast.LENGTH_LONG).show();
		}

	}

	public void registerWebService(StringEntity entity) {
		prgDialog.show();
		final String URL = "http://192.168.1.37:8080/PWsubastas/client/usuarios/add";
		AsyncHttpClient client = new AsyncHttpClient();
		client.post(null, URL, entity, "application/json",
				new AsyncHttpResponseHandler() {
					public void onSuccess(String response) {
						prgDialog.hide();
						setDefaultValues();
						Toast.makeText(getApplicationContext(),
								"Estás exitosamente registrado!",
								Toast.LENGTH_LONG).show();
					}
					public void onFailure(int statusCode, Throwable error,
							String content) {
						prgDialog.hide();
						if (statusCode == 404) {
							Toast.makeText(getApplicationContext(),
									"Petición de recurso no encontrado",
									Toast.LENGTH_LONG).show();
						} else if (statusCode == 500) {
							Toast.makeText(getApplicationContext(),
									"Algo salió mal en el server end",
									Toast.LENGTH_LONG).show();
						} else {
							Toast.makeText(
									getApplicationContext(),
									"Error inesperado, el servidor podría estar apagado, error de conexión",
									Toast.LENGTH_LONG).show();
						}
					}
				});
	}

	public void navigatetoLoginActivity(View view) {
		Intent loginIntent = new Intent(getApplicationContext(),
				LoginActivity.class);
		// Clears History of Activity
		loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(loginIntent);
	}

	public void setDefaultValues() {
		nameET.setText("");
		addressET.setText("");
		pwdET.setText("");
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.register, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
